<?php

namespace Tests\Feature;

use App\Http\Controllers\ResponseStatus;
use App\Utils\LargeProducerDataGenerator;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class LaargeProducerShopTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testSuccessIndexCode()
    {
        $response = $this->get('/api/shop/largeProducer');

        $response->assertStatus(200);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testSuccessIndexContentType()
    {
        $response = $this->get('/api/shop/largeProducer');
        $content = json_decode($response->getContent());

        self::assertTrue(
            count($content->data) === count(LargeProducerDataGenerator::SUCCESS_DATA['data']['products'])
        );
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testErrorIndexCode()
    {
        $response = $this->get('/api/shop/largeProducer?status=error');
        $response->assertStatus(500);
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testErrorIndexContent()
    {
        $response = $this->get('/api/shop/largeProducer?status=error');
        $content = json_decode($response->getContent());
        $errorResponseData = LargeProducerDataGenerator::ERROR_DATA['data'];

        self::assertTrue(
            $content->message === $errorResponseData['message']
            && $content->status === ResponseStatus::ERROR
        );
    }
}
