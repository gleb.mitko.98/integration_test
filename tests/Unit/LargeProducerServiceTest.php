<?php

namespace Tests\Unit;

use App\Exceptions\ShopExceptions\LargeProducer\GetAllException;
use App\Http\Controllers\ResponseStatus;
use Illuminate\Contracts\Container\BindingResolutionException;
use App\Services\Shops\LargeProducer\{LargeProducerService, LargeProducerServiceJsonImpl};
use App\Utils\LargeProducerDataGenerator;
use PHPUnit\Framework\TestCase;
use function PHPUnit\Framework\assertTrue;

/**
 * Тест серивиса LargeProducerService в иссполнении LargeProducerServiceJsonImpl
 *
 * Class LargeProducerServiceTest
 * @package Tests\Unit
 */
class LargeProducerServiceTest extends TestCase
{
    /**
     * Тест метода получение списка с успешным статусом
     *
     * @return void
     * @throws BindingResolutionException
     */
    public function testSuccessGetAll()
    {
        app()->bind(LargeProducerService::class, LargeProducerServiceJsonImpl::class);
        $largeProducerService = app()->make(LargeProducerService::class);

        $data = $largeProducerService->getAll(ResponseStatus::SUCCESS);

        $this->assertTrue(
            count($data) === count(LargeProducerDataGenerator::SUCCESS_DATA['data']['products'])
        );
    }

    /**
     * Тест метожа получения списка с неуспешным статусом
     */
    public function testErrorGetAll()
    {
        try {
            app()->bind(LargeProducerService::class, LargeProducerServiceJsonImpl::class);
            $largeProducerService = app()->make(LargeProducerService::class);

            $largeProducerService->getAll(ResponseStatus::ERROR);
        } catch (\Exception $e) {
            assertTrue($e instanceof GetAllException);
        }
    }
}
