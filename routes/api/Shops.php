<?php

use App\Http\Controllers\API\Shops\LargeProducerController;
use Illuminate\Support\Facades\Route;

Route::prefix('shop')->group(function () {
    Route::apiResource('largeProducer', LargeProducerController::class);
});
