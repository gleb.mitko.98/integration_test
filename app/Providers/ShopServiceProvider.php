<?php

namespace App\Providers;

use App\Services\Shops\LargeProducer\{LargeProducerService, LargeProducerServiceJsonImpl};
use Illuminate\Support\ServiceProvider;

/**
 * Провайдер для регистрации реализации сервисов для магазинов
 *
 * Class ShopServiceProvider
 * @package App\Providers
 */
class ShopServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(LargeProducerService::class, LargeProducerServiceJsonImpl::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
