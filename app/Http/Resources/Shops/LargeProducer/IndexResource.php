<?php

namespace App\Http\Resources\Shops\LargeProducer;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class IndexResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'vendorCode' => $this->vendor_code,
            'name' => $this->name,
            'price' => $this->price,
            'features' => $this->features,
        ];
    }
}
