<?php

namespace App\Http\Controllers;

/**
 * Класс с заголовками ответа
 *
 * Class ResponseHeaders
 * @package App\Http\Controllers
 */
class ResponseHeaders
{
    /**
     * Хэдеры для json ответа
     *
     * @var array
     */
    public const JSON_HEADERS = [
        'Content-Type' => 'application/json; charset=utf-8'
    ];
}
