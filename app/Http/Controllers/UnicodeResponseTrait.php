<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;

trait UnicodeResponseTrait
{
    /**
     * @var array
     *
     * For response
     */
    private array $response = [];

    /**
     * Статус ответа
     *
     * @var string
     */
    private string $status;

    /**
     * Код ответа
     *
     * @var string
     */
    private string $code;

    /**
     * @param array $data
     * @return $this
     *
     * Добавить данные
     */
    public function success($data = [])
    {
        $this->response = [
            'data' => $data
        ];

        $this->status = ResponseStatus::SUCCESS;
        $this->code = JsonResponse::HTTP_OK;
        return $this;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     *
     * Сформмировать ответ к отправке
     */
    public function send()
    {
        return response()
            ->json(
                array_merge($this->response, [
                    'status' => $this->status
                ]),
                $this->code,
                ResponseHeaders::JSON_HEADERS,
                JSON_UNESCAPED_UNICODE);
    }
}
