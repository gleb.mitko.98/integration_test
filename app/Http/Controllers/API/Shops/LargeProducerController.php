<?php

namespace App\Http\Controllers\API\Shops;

use App\Http\Controllers\{Controller, ResponseStatus};
use App\Http\Requests\Shops\LargeProducer\IndexRequest;
use App\Http\Resources\Shops\LargeProducer\IndexResource;
use App\Services\Shops\LargeProducer\LargeProducerService;
use Illuminate\Http\{Request, JsonResponse};

/**
 * Контроллер для взаимодействия с магазином КРУПНЫЙ ПОСТАВЩИК
 *
 * Class LargeProducerController
 * @package App\Http\Controllers\API\Shops
 */
class LargeProducerController extends Controller
{
    /**
     * Сервис для работы с магазином КРУПНЫЙ ПОСТАВЩИК
     *
     * @var LargeProducerService
     */
    protected LargeProducerService $largeProducerService;

    /**
     * LargeProducerController constructor.
     * @param LargeProducerService $largeProducerService
     */
    public function __construct(LargeProducerService $largeProducerService)
    {
        $this->largeProducerService = $largeProducerService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param IndexRequest $request
     * @return JsonResponse
     */
    public function index(IndexRequest $request): JsonResponse
    {
        $data = $this->largeProducerService->getAll(
            $request->get('status', ResponseStatus::SUCCESS)
        );

        return $this->success(
            IndexResource::collection($data)
        )->send();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        return $this->success()->send();
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        return $this->success()->send();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(Request $request, int $id): JsonResponse
    {
        return $this->success()->send();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        return $this->success()->send();
    }
}
