<?php

namespace App\Http\Controllers;

/**
 * Класс со статусами ответа
 *
 * Class ResponseStatus
 * @package App\Http\Controllers
 */
class ResponseStatus
{
    /**
     * Статус успеха
     *
     * @var string
     */
    public const SUCCESS = 'success';

    /**
     * Статус ошибки
     *
     * @var string
     */
    public const ERROR = 'error';
}
