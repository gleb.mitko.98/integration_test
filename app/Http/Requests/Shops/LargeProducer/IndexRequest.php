<?php

namespace App\Http\Requests\Shops\LargeProducer;

use App\Http\Controllers\ResponseStatus;
use App\Http\Requests\ApiFormRequest;
use Illuminate\Validation\Rule;

class IndexRequest extends ApiFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'status' => [
                'nullable',
                'string',
                Rule::in([
                    ResponseStatus::SUCCESS,
                    ResponseStatus::ERROR
                ])
            ]
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
}
