<?php

namespace App\Http\Requests;

use Illuminate\Http\JsonResponse;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Foundation\Http\FormRequest as LaravelFormRequest;

abstract class ApiFormRequest extends LaravelFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    abstract public function rules();

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    abstract public function authorize();

    /**
     * @param string $message
     */
    protected function fail(string $message)
    {
        throw new HttpResponseException(response()->json([
            'exception' => $message,
            'message' => 'Ошибка заполнения.',
            'status' => 'error'
        ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY));
    }

    /**
     * Handle a failed validation attempt.
     *
     * @param \Illuminate\Contracts\Validation\Validator $validator
     * @return void
     *
     */
    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'exception' => $validator->errors(),
            'message' => 'Ошибка заполнения.',
            'status' => 'error'
        ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY));
    }
}
