<?php

namespace App\Exceptions\ShopExceptions\LargeProducer;

use App\Exceptions\ShopExceptions\BaseShopException;

/**
 * Ошибка при получении всех записей магазина Крупного Поставщика
 *
 * Class GetAllException
 * @package App\Exceptions\ShopExceptions\LargeProducer
 */
class GetAllException extends BaseShopException
{
    //
}
