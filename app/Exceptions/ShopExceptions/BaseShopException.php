<?php

namespace App\Exceptions\ShopExceptions;

use Illuminate\Support\Facades\Log;
use Illuminate\Http\{JsonResponse, Request};
use Exception;

/**
 * Базовое исключение при работе с магазином
 *
 * Class BaseShopException
 * @package App\Exceptions\ShopExceptions
 */
class BaseShopException extends Exception
{
    /**
     * Report the exception.
     *
     * @return void
     */
    public function report(): void
    {
        Log::error($this->getMessage(), ['exception' => $this]);
    }

    /**
     * Render the exception into an HTTP response.
     *
     * @return JsonResponse
     */
    public function render(): JsonResponse
    {
        return response()
            ->json([
                'message' => $this->getMessage(),
                'status' => 'error'
            ],
                JsonResponse::HTTP_INTERNAL_SERVER_ERROR,
                ['Content-Type' => 'application/json; charset=utf-8'],
                JSON_UNESCAPED_UNICODE
            );
    }
}
