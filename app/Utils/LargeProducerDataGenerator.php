<?php

namespace App\Utils;

/**
 * Класс утилита для получения данных из магазина крупного поставщика
 *
 * Class LargeProducerDataGenerator
 * @package App\Utils
 */
class LargeProducerDataGenerator
{
    /**
     * @var array
     */
    public const SUCCESS_DATA = [
        'data' => [
            'products' => [
                [
                    'vendor_code' => 'VNDRCD353I',
                    'name' => 'Товар 1',
                    'price' => 100,
                    'features' => [
                        'feature1' => 100,
                        'feature2' => 'some text'
                    ]
                ],
                [
                    'vendor_code' => 'VNDRCD354I',
                    'name' => 'Товар 2',
                    'price' => 101,
                    'features' => [
                        'feature1' => 101,
                        'feature2' => 'some text'
                    ]
                ],
                [
                    'vendor_code' => 'VNDRCD355I',
                    'name' => 'Товар 3',
                    'price' => 102,
                    'features' => [
                        'feature1' => 102,
                        'feature2' => 'some text'
                    ]
                ],
                [
                    'vendor_code' => 'VNDRCD356I',
                    'name' => 'Товар 4',
                    'price' => 103,
                    'features' => [
                        'feature1' => 103,
                        'feature2' => 'some text'
                    ]
                ],
            ]
        ],
        'success' => true
    ];

    /**
     * @var array
     */
    public const ERROR_DATA = [
        'data' => [
            'message' => 'string error message',
            'code' => 'string error code'
        ],
        'success' => false
    ];

    /**
     * Получить данные со статусом успеха
     *
     * @return string
     */
    public static function getSuccessDataList(): string
    {
        return json_encode(self::SUCCESS_DATA, JSON_UNESCAPED_UNICODE);
    }

    /**
     *  Получить даанные со статусом ошибки
     *
     * @return string
     */
    public static function getErrorDataList(): string
    {
        return json_encode(self::ERROR_DATA, JSON_UNESCAPED_UNICODE);
    }
}
