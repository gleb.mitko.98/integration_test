<?php

namespace App\Services\Shops\LargeProducer;

use App\Exceptions\ShopExceptions\LargeProducer\GetAllException;
use App\Http\Controllers\ResponseStatus;
use App\Utils\LargeProducerDataGenerator;

/**
 * Реализация сервиса работы с поставщиком через Json
 *
 * Class LargeProducerServiceJsonImpl
 * @package App\Services\Shops\LargeProducer
 */
class LargeProducerServiceJsonImpl implements LargeProducerService
{
    /**
     * @inheritDoc
     *
     * @return mixed|void
     * @throws GetAllException
     */
    public function getAll(string $status)
    {
        switch ($status) {
            case ResponseStatus::SUCCESS:
                $data = LargeProducerDataGenerator::getSuccessDataList();
                break;
            case ResponseStatus::ERROR:
                $data = LargeProducerDataGenerator::getErrorDataList();
                break;
            default:
                return [];
        }

        $data = json_decode($data);

        if (!$data->success) throw new GetAllException($data->data->message);

        return $data->data->products;
    }
}
