<?php

namespace App\Services\Shops\LargeProducer;

/**
 * Интерфес для работы с магазином КРУПНЫЙ ПОСТАВЩИК
 *
 * Interface LargeProducerService
 * @package App\Services\LargeProducer
 */
interface LargeProducerService
{
    /**
     * Получить список всех записей
     *
     * @return mixed
     */
    public function getAll(string $status);
}
